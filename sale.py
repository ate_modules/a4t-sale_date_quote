# -*- coding: utf-8 -*-
# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.model import Workflow, ModelView, fields
from trytond.pyson import Eval

__all__ = ['Sale']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    quotation_date = fields.Date('Quotation Date',
        states={
            'readonly': ~Eval('state').in_(['draft', 'quotation']),
            'required': ~Eval('state').in_(['draft', 'quotation', 'cancel']),
            },
        depends=['state'])

    @classmethod
    def copy(cls, sales, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('quotation_date', None)
        return super(Sale, cls).copy(sales, default=default)

    @classmethod
    def set_quotation_date(cls, sales):
        Date = Pool().get('ir.date')
        for sale in sales:
            if not sale.quotation_date:
                cls.write([sale], {
                    'quotation_date': Date.today(),
                    })

    @classmethod
    @ModelView.button
    @Workflow.transition('quotation')
    def quote(cls, sales):
        cls.set_quotation_date(sales)
        super(Sale, cls).quote(sales)
