a4t_sale_date_quote
===================

Simply add a quote date on the sale object. This date will be used when the
quote is printing to be able to specify a validity period of a quotation.

Installing
----------

See INSTALL

Support
-------

If you encounter any problems with this module, please don't hesitate to ask
questions on the Adiczion support mail address :
  
  support@adiczion.net

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

  http://bugs.tryton.org/
  http://groups.tryton.org/
  http://wiki.tryton.org/
  irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

For more information about us please visit the Adiczion web site:

  http://adiczion.com

For more information about Tryton please visit the Tryton web site:

  http://www.tryton.org/