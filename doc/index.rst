Sale Date Quote
###############

Simply add a quote date on the sale object. This date will be used when the
quote is printing to be able to specify a validity period of a quotation 
(see 'sale.odt' file for sample).

- Date Quote: The date of the quotation.


